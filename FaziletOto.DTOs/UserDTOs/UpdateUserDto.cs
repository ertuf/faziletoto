﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.UserDTOs
{
    public class UpdateUserDto : AddUserDto
    {
        [Required]
        public int ID { get; set; }

        [DisplayName("Aktif")]
        public bool IsActive { get; set; }
    }
}
