﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.UserDTOs
{
    public class LoginUserDto
    {
        [Required(ErrorMessage = "E-posta adresini girmek zorunludur")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Şifre girmek zorunludur")]
        public string Password { get; set; }
    }
}
