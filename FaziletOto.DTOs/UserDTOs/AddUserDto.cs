﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DTOs.UserDTOs
{
    public class AddUserDto
    {
        [MaxLength(75,ErrorMessage ="İsim en fazla 75 karakter içerebilir")]
        [MinLength(2, ErrorMessage = "İsim en az 2 karakter içerebilir")]
        [Required(ErrorMessage ="Kullanıcı için isim girişi zorunludur")]
        [DisplayName("İsim")]
        public string Name { get; set; }

        [MaxLength(75, ErrorMessage = "Soyisim en fazla 75 karakter içerebilir")]
        [MinLength(2, ErrorMessage = "Soyisim en az 2 karakter içerebilir")]
        [Required(ErrorMessage = "Kullanıcı için soyisim girişi zorunludur")]
        [DisplayName("Soyisim")]
        public string Surname { get; set; }

        [MaxLength(15, ErrorMessage = "Kullanıcı adı en fazla 15 karakter içerebilir")]
        [MinLength(8, ErrorMessage = "Kullanıcı adı en az 8 karakter içerebilir")]
        [Required(ErrorMessage = "Kullanıcı için kullanıcı adı girişi zorunludur")]
        [DisplayName("Kullanıcı Adı")]
        public string Username { get; set; }

        [MaxLength(100, ErrorMessage = "E-posta addresi en fazla 100 karakter içerebilir")]
        [MinLength(10, ErrorMessage = "E-posta addresi en az 10 karakter içerebilir")]
        [Required(ErrorMessage = "Kullanıcı için E-posta addresi girişi zorunludur")]
        [DataType(DataType.EmailAddress, ErrorMessage ="E-posta adresinizi kontrol ediniz")]
        [DisplayName("E-posta Adresi")]
        public string Email { get; set; }

        [MaxLength(20,ErrorMessage = "Şifre en fazla 20 karakter içerebilir")]
        [MinLength(8, ErrorMessage = "Şifre en az 8 karakter içerebilir")]
        [Required(ErrorMessage = "Kullanıcı için Şifre girişi zorunludur")]
        [DataType(DataType.Password, ErrorMessage = "Şifrenizi kontrol ediniz")]
        [DisplayName("Şifre")]
        public string Password { get; set; }

        [MaxLength(20, ErrorMessage = "Şifre (Tekrar) en fazla 20 karakter içerebilir")]
        [MinLength(8, ErrorMessage = "Şifre (Tekrar) en az 10 karakter içerebilir")]
        [Required(ErrorMessage = "Kullanıcı için Şifre (Tekrar) girişi zorunludur")]
        [DataType(DataType.Password, ErrorMessage = "Şifre (Tekrar) kontrol ediniz")]
        [Compare("Password",ErrorMessage ="Şifreler eşleşmedi")]
        [DisplayName("Şifre (Tekrar)")]
        public string PasswordCheck { get; set; }

        [DisplayName("Admin")]
        public bool IsAdmin { get; set; }

        [DisplayName("Satış Personeli")]
        public bool IsSalesman { get; set; }

        [DisplayName("Stok Personeli")]
        public bool IsStockKeeper { get; set; }
    }
}
