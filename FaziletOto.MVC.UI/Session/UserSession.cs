﻿using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaziletOto.MVC.UI.Session
{
    public static class UserSession
    {
        public static User CurrentUser { get; set; }
    }
}