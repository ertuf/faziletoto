﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaziletOto.MVC.UI.Controllers
{
    public class CommonController : Controller
    {

        private readonly IUserService userManager;

        public CommonController(IUserService _userManager)
        {
            userManager = _userManager;
        }

        public PartialViewResult RenderNavbar()
        {
            User user = null;
            if (Request.Cookies.Get("FaziletOto_UserLoginKey") != null)
            {
                string loginKeyS = Request.Cookies.Get("FaziletOto_UserLoginKey").Value;
                Guid loginKey = Guid.Parse(loginKeyS);
                var loginReturn = userManager.Get(x => x.LoginKey == loginKey);
                user = loginReturn.Data;
            }
            return PartialView("~/Views/Shared/NavbarPartial.cshtml", user);
        }
    }
}