﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaziletOto.MVC.UI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService userManager;

        public AccountController(IUserService _usermanager)
        {
            userManager = _usermanager;
        }

        [HttpPost]
        public ActionResult LoginPost(LoginUserDto loginUserDto)
        {
            if (ModelState.IsValid)
            {
                var loginReturn = userManager.Login(loginUserDto.Email, loginUserDto.Password);
                if (loginReturn.ReturnTitle == ReturnTitle.Success)
                {
                    User user = loginReturn.Data;
                    user.LoginKey = Guid.NewGuid();
                    var updateReturn = userManager.Update(user);
                    if (updateReturn.ReturnTitle == ReturnTitle.Success)
                    {
                        //ID
                        HttpCookie cookieID = new HttpCookie("FaziletOto_UserID");
                        cookieID.Value = user.ID.ToString();
                        cookieID.Expires = DateTime.Now.AddDays(5);
                        Response.Cookies.Add(cookieID);
                        //Username
                        HttpCookie cookieUsername = new HttpCookie("FaziletOto_Username");
                        cookieUsername.Value = user.Username.ToString();
                        cookieUsername.Expires = DateTime.Now.AddDays(5);
                        Response.Cookies.Add(cookieUsername);
                        //LoginKey
                        HttpCookie cookieKey = new HttpCookie("FaziletOto_UserLoginKey");
                        cookieKey.Value = user.LoginKey.ToString();
                        cookieKey.Expires = DateTime.Now.AddDays(5);
                        Response.Cookies.Add(cookieKey);
                    }
                }
            }
            return RedirectToAction("HomePage", "Home");
        }
    }
}