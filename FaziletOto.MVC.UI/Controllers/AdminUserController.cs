﻿using AutoMapper;
using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FaziletOto.MVC.UI.Controllers
{
    public class AdminUserController : Controller
    {
        private readonly IUserService userManager;

        public AdminUserController(IUserService _userManager)
        {
            userManager = _userManager;
        }

        [HttpGet]
        public ActionResult AdminUserPage()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AdminUserAddPage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminUserAddPage(AddUserDto addUserDto)
        {
            if (ModelState.IsValid)
            {
                User user = Mapper.Map<User>(addUserDto);
                var baseReturn = userManager.AddUserAsAdmin(user);
                return RedirectToAction("AdminUserPage", "AdminUser");        
            }
            return View(addUserDto);
        }

        [HttpGet]
        public ActionResult AdminUserArrangePage()
        {
            var model = userManager.Gets();
            return View(model.Data);
        }

        [HttpPost]
        public JsonResult AdminUserUpdateAjax(UpdateUserDto updateUserDto)
        {
            if (ModelState.IsValid)
            {
                User updateUser = Mapper.Map<User>(updateUserDto);
                var json = userManager.UpdateUserAsAdmin(updateUser);
                return Json(json);
            }
            else
            {
                var json = new Dictionary<string, object>();
                json.Add("Result", false);
                return Json(json);
            }
        }

        [HttpPost]
        public JsonResult AdminUserDeleteAjax(int ID)
        {
            var baseResult = userManager.UpdateDeletion(userManager.Get(x => x.ID == ID).Data);
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("ID", ID.ToString());
            return Json(dictionary);
        }
    }
}