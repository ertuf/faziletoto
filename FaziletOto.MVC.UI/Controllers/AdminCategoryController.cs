﻿using AutoMapper;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.CategoryDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.Mvc;

namespace FaziletOto.MVC.UI.Controllers
{
    public class AdminCategoryController : Controller
    {
        [HttpGet]
        public ActionResult AdminCategoryPage()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AdminCategoryAddPage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminCategoryAddPage(AddProductCategoryDto addProductCategoryDto)
        {
            ProductCategory category = Mapper.Map<ProductCategory>(addProductCategoryDto);
            // TODO : after user
            return RedirectToAction("AdminCategoryPage", "AdminCategory");
        }

        [HttpGet]
        public ActionResult AdminCategoryArrangePage()
        {
            return View();
        }
    }
}