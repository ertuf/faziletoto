﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Manager
{
    public class ProductManager : GenericManager<Product, IProductDal>, IProductService
    {
        public ProductManager(IProductDal productDal) : base(productDal)
        {

        }
    }
}
