﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Contants;
using FaziletOto.CORE.Necessity;
using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DAL.Data.EF.Concrete;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Manager
{
    public class UserManager : GenericManager<User, IUserDal>, IUserService
    {
        public UserManager(IUserDal userDal) : base (userDal)
        {

        }

        public BaseReturn<User> AddUserAsAdmin(User user)
        {
            user.IsApproved = true;
            user.ApiKey = Guid.NewGuid();
            return EntityDal.Insert(user);
        }

        public Dictionary<string,object> UpdateUserAsAdmin(User updateUser)
        {
            Dictionary<string, object> json = new Dictionary<string, object>();
            var baseReturn = EntityDal.Get(x => x.ID == updateUser.ID);
            if (baseReturn.ReturnTitle == ReturnTitle.Success)
            {
                User user = baseReturn.Data;
                user.Name = updateUser.Name;
                user.Surname = updateUser.Surname;
                user.Username = updateUser.Username;
                user.Email = updateUser.Email;
                user.Password = updateUser.Password;
                user.IsActive = updateUser.IsActive;
                user.IsAdmin = updateUser.IsAdmin;
                user.IsSalesman = updateUser.IsSalesman;
                user.IsStockKeeper = updateUser.IsStockKeeper;
                var updateReturn = EntityDal.Update(user);
                if (updateReturn.ReturnTitle == ReturnTitle.Success)
                {
                    json.Add("Result", true);
                    json.Add("ID", user.ID);
                }
                else
                {
                    json.Add("Result", false);
                }
            }
            else
            {
                json.Add("Result", false);
            }
            return json;
        }

        public BaseReturn<User> Login(string email, string password)
        {
            var baseReturn = EntityDal.Get(x => x.Email == email && x.Password == password);
            return baseReturn;
        }
    }
}
