﻿using FaziletOto.BLL.Data.EF.Service;
using FaziletOto.CORE.Data.EF.Abstract;
using FaziletOto.CORE.Data.EF.Concrete;
using FaziletOto.CORE.Model;
using FaziletOto.CORE.Necessity;
using FaziletOto.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Manager
{
    public class GenericManager<TEntity, IEntityDal> : IGenericService<TEntity>
        where TEntity : BaseEntity
        where IEntityDal : IRepository<TEntity>
    {
        protected IEntityDal EntityDal { get; set; }

        public GenericManager(IEntityDal entityDal)
        {
            EntityDal = entityDal;
        }

        public BaseReturn<TEntity> Insert(TEntity entity)
        {
            return EntityDal.Insert(entity);
        }

        public BaseReturn Update(TEntity entity)
        {
            return EntityDal.Update(entity);
        }

        public BaseReturn UpdateActivation(TEntity entity, bool isActive)
        {
            return EntityDal.UpdateActivation(entity, isActive);
        }

        public BaseReturn UpdateDeletion(TEntity entity)
        {
            return EntityDal.UpdateDeletion(entity);
        }

        public BaseReturn<TEntity> Get(Expression<Func<TEntity, bool>> filter, params string[] tables)
        {
            return EntityDal.Get(filter, tables);
        }

        public BaseReturn<List<TEntity>> Gets(Expression<Func<TEntity, bool>> filter = null, params string[] tables)
        {
            return EntityDal.Gets(filter, tables);

        }
    }
}
