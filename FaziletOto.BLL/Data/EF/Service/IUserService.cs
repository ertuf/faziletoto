﻿using FaziletOto.CORE.Necessity;
using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.BLL.Data.EF.Service
{
    public interface IUserService : IUserDal
    {
        BaseReturn<User> AddUserAsAdmin(User user);

        Dictionary<string, object> UpdateUserAsAdmin(User updateUser);

        BaseReturn<User> Login(string email, string password);
    }
}
