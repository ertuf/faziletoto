﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Constants
{
    public enum MeasureType
    {
        mL_Mililitre, l_Litre, gr_Gram, kg_Kilogram 
    }
}
