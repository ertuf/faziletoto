﻿using FaziletOto.CORE.Model;
using FaziletOto.DOMAIN.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public float StockAmount { get; set; }
        public float StandartStockAmount { get; set; }
        public MeasureType MeasureType { get; set; }
        public int MaxOrderAmount { get; set; }
        public decimal Price { get; set; }
        public double Discount { get; set; }

        //one to many parent
        public List<BasketItem> BasketItems { get; set; }

        //one to many child
        public int AddingUserID { get; set; }
        public User AddingUser { get; set; }

        //many to many
        public List<ProductCategory> ProductCategories { get; set; }
        //one to one
    }
}
