﻿using FaziletOto.CORE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Entities
{
    public class User : BaseEntity 
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Guid ApiKey { get; set; }
        public Guid LoginKey { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsSalesman { get; set; }
        public bool IsStockKeeper { get; set; }
        public bool IsApproved { get; set; }

        //one to many parent
        public List<Customer> EvaluatedCustomers { get; set; }
        public List<Basket> EvaluatedBaskets { get; set; }
        public List<BasketItem> EvaluatedBasketItems { get; set; }
        public List<ProductCategory> AddedProductCategories { get; set; }
        public List<Product> AddedProducts { get; set; }
        
        //one to many child
        //many to many
        //one to one
    }
}
