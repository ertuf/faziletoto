﻿using FaziletOto.CORE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DOMAIN.Entities
{
    public class Customer : BaseEntity
    {
        public string ContactPersonnel { get; set; }
        public string ContactPhone1 { get; set; }
        public string ContactPhone2 { get; set; }
        public string ContactAddress { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public Guid ApiKey { get; set; }
        public Guid LoginKey { get; set; }
        public string ContactCity { get; set; }
        public string ContactCountry { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone1 { get; set; }
        public string CompanyPhone2 { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyCountry { get; set; }
        public double CompanyDiscount { get; set; }        
        public bool CanBuy { get; set; }
        public bool CanText { get; set; }
        public bool IsApproved { get; set; }

        //one to many parent
        public List<BasketItem> AddedBasketItems { get; set; }
        public List<Basket> AddedBaskets { get; set; }

        //one to many child
        public int EvaluatingUserID { get; set; }
        public User EvaluatingUser { get; set; }

        //many to many
        //one to one
    }
}
