﻿using FaziletOto.DAL.Data.EF.Abstract;
using FaziletOto.DAL.Data.EF.Concrete;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.INFRASTRUCTURE.DependencyModules
{
    public class DependencyBll : NinjectModule
    {
        public override void Load()
        {
            Bind<IBasketItemDal>().To<BasketItemDal>().InSingletonScope();
            Bind<IBasketDal>().To<BasketDal>().InSingletonScope();
            Bind<ICustomerDal>().To<CustomerDal>().InSingletonScope();
            Bind<IProductCategoryDal>().To<ProductCategoryDal>().InSingletonScope();
            Bind<IProductDal>().To<ProductDal>().InSingletonScope();
            Bind<IUserDal>().To<UserDal>().InSingletonScope();
        }
    }
}
