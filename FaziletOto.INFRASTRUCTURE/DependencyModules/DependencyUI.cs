﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaziletOto.BLL.Data.EF.Manager;
using FaziletOto.BLL.Data.EF.Service;
using Ninject.Modules;

namespace FaziletOto.INFRASTRUCTURE.DependencyModules
{
    public class DependencyUI : NinjectModule
    {
        public override void Load()
        {
            Bind<IBasketItemService>().To<BasketItemManager>().InSingletonScope();
            Bind<IBasketService>().To<BasketManager>().InSingletonScope();
            Bind<ICustomerService>().To<CustomerManager>().InSingletonScope();
            Bind<IProductCategoryService>().To<ProductCategoryManager>().InSingletonScope();
            Bind<IProductService>().To<ProductManager>().InSingletonScope();
            Bind<IUserService>().To<UserManager>().InSingletonScope();
        }
    }
}
