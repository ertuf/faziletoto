﻿using AutoMapper;
using FaziletOto.DOMAIN.Entities;
using FaziletOto.DTOs.CategoryDTOs;
using FaziletOto.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.INFRASTRUCTURE.AutoMapperProfiles
{
    public class AutoMapProfile : Profile
    {
        public AutoMapProfile()
        {
            // user profiles
            CreateMap<User, AddUserDto>();
            CreateMap<AddUserDto, User>();
            CreateMap<User, UpdateUserDto>();
            CreateMap<UpdateUserDto, User>();

            // productCategory profile
            CreateMap<AddProductCategoryDto, ProductCategory>();
        }
    }
}
