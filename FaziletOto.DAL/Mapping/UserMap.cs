﻿using FaziletOto.CORE.Mapping;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Mapping
{
    public class UserMap : BaseMap<User>
    {
        public UserMap()
        {
            ToTable("Users");
            Property(x => x.Name).IsRequired().HasMaxLength(75);
            Property(x => x.Surname).IsRequired().HasMaxLength(75);
            Property(x => x.Username).IsRequired().HasMaxLength(15);
            Property(x => x.Email).IsRequired().HasMaxLength(100);
            Property(x => x.Password).IsRequired().HasMaxLength(20);
            Property(x => x.ApiKey).IsRequired().HasColumnType("uniqueidentifier");
            Property(x => x.LoginKey).IsOptional().HasColumnType("uniqueidentifier");
            Property(x => x.IsAdmin).IsRequired();
            Property(x => x.IsSalesman).IsRequired();
            Property(x => x.IsStockKeeper).IsRequired();
            Property(x => x.IsApproved).IsRequired();

            //relations
        }
    }
}
