﻿using FaziletOto.CORE.Mapping;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Mapping
{
    public class BasketItemMap : BaseMap<BasketItem>
    {
        public BasketItemMap()
        {
            ToTable("BasketItems");
            Property(x => x.Quantity).IsRequired();
            Property(x => x.CustomerNote).IsOptional().HasMaxLength(200);
            Property(x => x.Situation).IsRequired();
            Property(x => x.TotalPrice).IsRequired().HasColumnType("money");

            //relations
            HasRequired(x => x.Product).WithMany(x => x.BasketItems).HasForeignKey(x => x.ProductID).WillCascadeOnDelete(false);
            HasOptional(x => x.EvaluatingUser).WithMany(x => x.EvaluatedBasketItems).HasForeignKey(x => x.EvaluatingUserID).WillCascadeOnDelete(false);
            HasRequired(x => x.AddingCustomer).WithMany(x => x.AddedBasketItems).HasForeignKey(x => x.AddingCustomerID).WillCascadeOnDelete(false);

            // x.basket in basketmap
        }
    }
}
