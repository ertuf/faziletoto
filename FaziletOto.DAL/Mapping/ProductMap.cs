﻿using FaziletOto.CORE.Mapping;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Mapping
{
    public class ProductMap : BaseMap<Product>
    {
        public ProductMap()
        {
            ToTable("Products");
            Property(x => x.Name).IsRequired().HasMaxLength(200);
            Property(x => x.Description).IsOptional().HasMaxLength(500);
            Property(x => x.StockAmount).IsRequired().HasColumnType("real");
            Property(x => x.StandartStockAmount).IsRequired().HasColumnType("real");
            Property(x => x.MeasureType).IsRequired();
            Property(x => x.MaxOrderAmount).IsOptional();
            Property(x => x.Price).IsRequired().HasColumnType("money");
            Property(x => x.Discount).IsRequired().HasColumnType("float");

            //relations
            HasRequired(x => x.AddingUser).WithMany(x => x.AddedProducts).HasForeignKey(x => x.AddingUserID).WillCascadeOnDelete(false);
            HasMany(x => x.ProductCategories).WithMany(x => x.Products).Map(x => x.MapLeftKey("ProductID").MapRightKey("ProducyCategoryID").ToTable("ProductAndCategory"));

            // x.basketItem in basketItems
        }
    }
}
