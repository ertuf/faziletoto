﻿using FaziletOto.CORE.Data.EF.Abstract;
using FaziletOto.DOMAIN.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.DAL.Data.EF.Abstract
{
    public interface ICustomerDal : IRepository<Customer>
    {
    }
}
