﻿namespace FaziletOto.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usernamrCust : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "Username", c => c.String(nullable: false, maxLength: 15));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "Username");
        }
    }
}
