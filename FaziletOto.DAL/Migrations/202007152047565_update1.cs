﻿namespace FaziletOto.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsStockKeeper", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "StandartStockAmount", c => c.Single(nullable: false));
            AddColumn("dbo.Products", "MeasureType", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "StockAmount", c => c.Single(nullable: false));
            DropColumn("dbo.Users", "IsStorekeeper");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "IsStorekeeper", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Products", "StockAmount", c => c.Int(nullable: false));
            DropColumn("dbo.Products", "MeasureType");
            DropColumn("dbo.Products", "StandartStockAmount");
            DropColumn("dbo.Users", "IsStockKeeper");
        }
    }
}
