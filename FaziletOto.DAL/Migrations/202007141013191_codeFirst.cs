﻿namespace FaziletOto.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class codeFirst : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BasketItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        CustomerNote = c.String(maxLength: 200),
                        Situation = c.Int(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, storeType: "money"),
                        ProductID = c.Int(nullable: false),
                        EvaluatingUserID = c.Int(),
                        AddingCustomerID = c.Int(nullable: false),
                        BasketID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Baskets", t => t.BasketID)
                .ForeignKey("dbo.Customers", t => t.AddingCustomerID)
                .ForeignKey("dbo.Users", t => t.EvaluatingUserID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.EvaluatingUserID)
                .Index(t => t.AddingCustomerID)
                .Index(t => t.BasketID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ContactPersonnel = c.String(nullable: false, maxLength: 50),
                        ContactPhone1 = c.String(maxLength: 50),
                        ContactPhone2 = c.String(maxLength: 50),
                        ContactAddress = c.String(maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 20),
                        Key = c.Guid(nullable: false),
                        ContactCity = c.String(maxLength: 50),
                        ContactCountry = c.String(maxLength: 50),
                        CompanyName = c.String(nullable: false, maxLength: 200),
                        CompanyPhone1 = c.String(maxLength: 50),
                        CompanyPhone2 = c.String(maxLength: 50),
                        CompanyAddress = c.String(nullable: false, maxLength: 200),
                        CompanyCity = c.String(nullable: false, maxLength: 50),
                        CompanyCountry = c.String(nullable: false, maxLength: 50),
                        CompanyDiscount = c.Double(nullable: false),
                        CanBuy = c.Boolean(nullable: false),
                        CanText = c.Boolean(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        EvaluatingUserID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.EvaluatingUserID)
                .Index(t => t.EvaluatingUserID);
            
            CreateTable(
                "dbo.Baskets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TotalPrice = c.Decimal(nullable: false, storeType: "money"),
                        Situation = c.Int(nullable: false),
                        EvaluatingUserID = c.Int(),
                        AddedCustomerID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.AddedCustomerID)
                .ForeignKey("dbo.Users", t => t.EvaluatingUserID)
                .Index(t => t.EvaluatingUserID)
                .Index(t => t.AddedCustomerID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 75),
                        Surname = c.String(nullable: false, maxLength: 75),
                        Username = c.String(nullable: false, maxLength: 15),
                        Email = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 20),
                        Key = c.Guid(nullable: false),
                        IsAdmin = c.Boolean(nullable: false),
                        IsSalesman = c.Boolean(nullable: false),
                        IsStorekeeper = c.Boolean(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 500),
                        AddingUserID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.AddingUserID)
                .Index(t => t.AddingUserID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Description = c.String(maxLength: 500),
                        StockAmount = c.Int(nullable: false),
                        MaxOrderAmount = c.Int(),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                        Discount = c.Double(nullable: false),
                        AddingUserID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        InsertDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UpdateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DeleteDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.AddingUserID)
                .Index(t => t.AddingUserID);
            
            CreateTable(
                "dbo.ProductAndCategory",
                c => new
                    {
                        ProductID = c.Int(nullable: false),
                        ProducyCategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductID, t.ProducyCategoryID })
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.ProductCategories", t => t.ProducyCategoryID, cascadeDelete: true)
                .Index(t => t.ProductID)
                .Index(t => t.ProducyCategoryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BasketItems", "ProductID", "dbo.Products");
            DropForeignKey("dbo.BasketItems", "EvaluatingUserID", "dbo.Users");
            DropForeignKey("dbo.BasketItems", "AddingCustomerID", "dbo.Customers");
            DropForeignKey("dbo.Customers", "EvaluatingUserID", "dbo.Users");
            DropForeignKey("dbo.Baskets", "EvaluatingUserID", "dbo.Users");
            DropForeignKey("dbo.ProductAndCategory", "ProducyCategoryID", "dbo.ProductCategories");
            DropForeignKey("dbo.ProductAndCategory", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Products", "AddingUserID", "dbo.Users");
            DropForeignKey("dbo.ProductCategories", "AddingUserID", "dbo.Users");
            DropForeignKey("dbo.BasketItems", "BasketID", "dbo.Baskets");
            DropForeignKey("dbo.Baskets", "AddedCustomerID", "dbo.Customers");
            DropIndex("dbo.ProductAndCategory", new[] { "ProducyCategoryID" });
            DropIndex("dbo.ProductAndCategory", new[] { "ProductID" });
            DropIndex("dbo.Products", new[] { "AddingUserID" });
            DropIndex("dbo.ProductCategories", new[] { "AddingUserID" });
            DropIndex("dbo.Baskets", new[] { "AddedCustomerID" });
            DropIndex("dbo.Baskets", new[] { "EvaluatingUserID" });
            DropIndex("dbo.Customers", new[] { "EvaluatingUserID" });
            DropIndex("dbo.BasketItems", new[] { "BasketID" });
            DropIndex("dbo.BasketItems", new[] { "AddingCustomerID" });
            DropIndex("dbo.BasketItems", new[] { "EvaluatingUserID" });
            DropIndex("dbo.BasketItems", new[] { "ProductID" });
            DropTable("dbo.ProductAndCategory");
            DropTable("dbo.Products");
            DropTable("dbo.ProductCategories");
            DropTable("dbo.Users");
            DropTable("dbo.Baskets");
            DropTable("dbo.Customers");
            DropTable("dbo.BasketItems");
        }
    }
}
