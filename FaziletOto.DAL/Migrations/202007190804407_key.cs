﻿namespace FaziletOto.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class key : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "ApiKey", c => c.Guid(nullable: false));
            AddColumn("dbo.Users", "LoginKey", c => c.Guid());
            AlterColumn("dbo.ProductCategories", "Description", c => c.String(maxLength: 500));
            DropColumn("dbo.Users", "Key");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Key", c => c.Guid(nullable: false));
            AlterColumn("dbo.ProductCategories", "Description", c => c.String(nullable: false, maxLength: 500));
            DropColumn("dbo.Users", "LoginKey");
            DropColumn("dbo.Users", "ApiKey");
        }
    }
}
