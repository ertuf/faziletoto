﻿namespace FaziletOto.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class customerkeys : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "ApiKey", c => c.Guid(nullable: false));
            AddColumn("dbo.Customers", "LoginKey", c => c.Guid());
            DropColumn("dbo.Customers", "Key");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "Key", c => c.Guid(nullable: false));
            DropColumn("dbo.Customers", "LoginKey");
            DropColumn("dbo.Customers", "ApiKey");
        }
    }
}
