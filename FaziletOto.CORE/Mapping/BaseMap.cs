﻿using FaziletOto.CORE.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace FaziletOto.CORE.Mapping
{
    public class BaseMap<TEntity> : EntityTypeConfiguration<TEntity>
        where TEntity : BaseEntity
    {
        public BaseMap()
        {
            HasKey(x => x.ID);
            Property(x => x.IsActive).IsRequired();
            Property(x => x.IsDeleted).IsRequired();
            Property(x => x.InsertDate).IsRequired().HasColumnType("datetime2");
            Property(x => x.UpdateDate).IsRequired().HasColumnType("datetime2");
            Property(x => x.DeleteDate).HasColumnType("datetime2");
        }
    }
}
