﻿using FaziletOto.CORE.Model;
using FaziletOto.CORE.Necessity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.CORE.Data.EF.Abstract
{
    public interface IRepository<TEntity> 
        where TEntity : BaseEntity
    {
        BaseReturn<TEntity> Insert(TEntity entity);
        BaseReturn Update(TEntity entity);
        BaseReturn UpdateActivation(TEntity entity, bool isActive);
        BaseReturn UpdateDeletion(TEntity entity);
        BaseReturn<TEntity> Get(Expression<Func<TEntity,bool>> filter, params string[] tables);
        BaseReturn<List<TEntity>> Gets(Expression<Func<TEntity, bool>> filter=null, params string[] tables);

    }
}
