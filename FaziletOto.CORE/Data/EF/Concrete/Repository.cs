﻿using FaziletOto.CORE.Contants;
using FaziletOto.CORE.Data.EF.Abstract;
using FaziletOto.CORE.Model;
using FaziletOto.CORE.Necessity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FaziletOto.CORE.Data.EF.Concrete
{
    public class Repository<TEntity, TContext> : IRepository<TEntity>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        protected TContext Ctx { get; set; }
        protected DbSet<TEntity> Db { get; set; }

        public Repository(TContext ctx)
        {
            Ctx = ctx;
            Db = Ctx.Set<TEntity>();
        }

        public BaseReturn<TEntity> Insert(TEntity entity)
        {
            BaseReturn<TEntity> baseReturn = null;
            try
            {
                entity.IsActive = true;
                entity.IsDeleted = false;
                entity.InsertDate = DateTime.Now;
                entity.UpdateDate = DateTime.Now;
                TEntity returnEntity = Db.Add(entity);
                if (Ctx.SaveChanges() > 0 && returnEntity != null)
                {
                    baseReturn = new BaseReturn<TEntity>(returnEntity, ReturnTitle.Success, "Added");
                }
                else
                {
                    baseReturn = new BaseReturn<TEntity>(null, ReturnTitle.Warning, "Not Added");
                }
            }
            catch (Exception ex)
            {
                baseReturn = new BaseReturn<TEntity>(null, ReturnTitle.Error, ex.GetInnestException());
            }
            return baseReturn;
        }

        public BaseReturn Update(TEntity entity)
        {
            BaseReturn baseReturn = null;
            try
            {
                entity.UpdateDate = DateTime.Now;
                Db.AddOrUpdate(x => x.ID, entity);
                if (Ctx.SaveChanges() > 0)
                {
                    baseReturn = new BaseReturn(ReturnTitle.Success, "Updated");
                }
                else
                {
                    baseReturn = new BaseReturn(ReturnTitle.Warning, "Not Updated");
                }
            }
            catch (Exception ex)
            {
                baseReturn = new BaseReturn(ReturnTitle.Error, ex.GetInnestException());
            }
            return baseReturn;
        }

        public BaseReturn UpdateActivation(TEntity entity, bool isActive)
        {
            BaseReturn baseReturn = null;
            try
            {
                entity.IsActive = isActive;
                entity.UpdateDate = DateTime.Now;
                Db.AddOrUpdate(x=> x.ID, entity);
                if (Ctx.SaveChanges() > 0)
                {
                    if (isActive)
                    {
                        baseReturn = new BaseReturn(ReturnTitle.Success, "Activated");
                    }
                    else
                    {
                        baseReturn = new BaseReturn(ReturnTitle.Success, "Deactivated");
                    }
                }
                else
                {
                    baseReturn = new BaseReturn(ReturnTitle.Warning, "No Activation Change");
                }
            }
            catch (Exception ex)
            {
                baseReturn = new BaseReturn(ReturnTitle.Error, ex.GetInnestException());
            }
            return baseReturn;
        }

        public BaseReturn UpdateDeletion(TEntity entity)
        {
            BaseReturn baseReturn = null;
            try
            {
                entity.IsDeleted = true;
                entity.IsActive = false;
                entity.UpdateDate = DateTime.Now;
                Db.AddOrUpdate(x => x.ID, entity);
                if (Ctx.SaveChanges() > 0)
                {
                    baseReturn = new BaseReturn(ReturnTitle.Success, "Deleted");
                }
                else
                {
                    baseReturn = new BaseReturn(ReturnTitle.Warning, "Not Deleted");
                }
            }
            catch (Exception ex)
            {
                baseReturn = new BaseReturn(ReturnTitle.Error, ex.GetInnestException());
            }
            return baseReturn;
        }

        public BaseReturn<TEntity> Get(Expression<Func<TEntity, bool>> filter, params string[] tables)
        {
            BaseReturn<TEntity> baseReturn = null;
            TEntity returnEntity = null;
            try
            {
                IQueryable<TEntity> ExtendedDb = Db;
                foreach (var table in tables)
                {
                    ExtendedDb = ExtendedDb.Include(table);
                }
                returnEntity = ExtendedDb.FirstOrDefault(filter);
                if (returnEntity != null)
                {
                    baseReturn = new BaseReturn<TEntity>(returnEntity, ReturnTitle.Success, "Get");
                }
                else
                {
                    baseReturn = new BaseReturn<TEntity>(null, ReturnTitle.Warning, "Not Get");
                }
            }
            catch (Exception ex)
            {
                baseReturn = new BaseReturn<TEntity>(null, ReturnTitle.Error, ex.GetInnestException());
            }
            return baseReturn;
        }

        public BaseReturn<List<TEntity>> Gets(Expression<Func<TEntity, bool>> filter = null, params string[] tables)
        {
            BaseReturn<List<TEntity>> baseReturn = null;
            List<TEntity> returnEntities = null;
            try
            {
                IQueryable<TEntity> ExtentedDb = Db;
                foreach (var table in tables)
                {
                    ExtentedDb = ExtentedDb.Include(table);
                }

                if (filter == null)
                {
                    returnEntities = ExtentedDb.ToList();
                }
                else
                {
                    returnEntities = ExtentedDb.Where(filter).ToList();
                }

                if (returnEntities != null && returnEntities.Count > 0)
                {
                    baseReturn = new BaseReturn<List<TEntity>>(returnEntities, ReturnTitle.Success, "Gets");
                }
                else if (returnEntities.Count == 0)
                {
                    baseReturn = new BaseReturn<List<TEntity>>(returnEntities, ReturnTitle.Warning, "Gets Zero Element");
                }
                else
                {
                    baseReturn = new BaseReturn<List<TEntity>>(null, ReturnTitle.Warning, "Not Gets");
                }

            }
            catch (Exception ex)
            {
                baseReturn = new BaseReturn<List<TEntity>>(null, ReturnTitle.Error, ex.GetInnestException());
            }
            return baseReturn;
        }
    }
}
